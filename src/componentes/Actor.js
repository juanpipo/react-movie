import React from "react";
import Styled from "styled-components";


const urlbaseimage = "https://image.tmdb.org/t/p/w185";

const Container = Styled.div`
    border:2px, solid gold;
    margin:2px;
`
const Foto = Styled.img`
    
`
const Nombre = Styled.span`
    background:rgba(0,0,0,0.5);
    color:white;
`

export default ({actor}) => (
    <Container>
        <Foto src={actor.profile_path ? urlbaseimage+actor.profile_path : "http://dummyimage.com/185&text=no_image"}></Foto>
        <Nombre src={actor.name}></Nombre>
    </Container>
)