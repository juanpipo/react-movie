import React from 'react';
import {BrowserRouter,Route,Link} from 'react-router-dom'
import HomePage from '../paginas/HomePage'
import PeliculaDetalle from '../paginas/PeliculaDetalle'
import Header from './Header'
import Footer from './Footer'

function App() {
  return (
    <BrowserRouter>
    <div>
      <Header />
      <Route exact path="/" component={HomePage}/>
      <Route path="/detalles/:peliculaid" component={PeliculaDetalle}></Route>
      <Footer />
    </div>
    </BrowserRouter>
    
  );
}

export default App;
