import React from "react";
import Styled from "styled-components";

const urlbaseimage = "https://image.tmdb.org/t/p/w780";

const Inicio = Styled.div`
    background: url(${props =>
      urlbaseimage + props.pelicula.backdrop_path}) no-repeat;
    background-size:cover;
    height:700px;
    color: white;
    font-size:22px;
    font-family:"Anton", sans-serif;
    font-family: 'Anton', sans-serif;
`;

const PeliculaInfo = Styled.div`
    position:relative;
    color:rgba(285,220,57,1);
`;

const PeliculaInfoContent = Styled.div`
    position:absolute;
    top:150px;
    left:50px;
    background:rgba(38,50,56,0.5);
    padding:10px;
`;

const PeliculaInfoTitulo = Styled.h1`
    color:rgba(255,179,0,1)
    font-family:"Rock Salt", cursive;
`;

const PeliculaInfoOverview = Styled.p`
    font-size: 14px;
    max-width:300px;

`;

const VotoPromedio = Styled.span`
    color:rgba(100,221,23,1)
    font-family:"Cinzel",serif;
`;

const Cargando = Styled.div`
    text-align: center;
    font-size: 22px;
`;

export default ({ pelicula }) => {
  if (pelicula) {
    return (
      <div>
        <Inicio pelicula={pelicula}>
          <PeliculaInfo>
            <PeliculaInfoContent>
              <PeliculaInfoTitulo>{pelicula.title}</PeliculaInfoTitulo>
              <PeliculaInfoOverview>{pelicula.overview}</PeliculaInfoOverview>
              <VotoPromedio>{pelicula.vote_average}/10</VotoPromedio>
            </PeliculaInfoContent>
          </PeliculaInfo>
        </Inicio>
      </div>
    );
  } else {
    return <Cargando />;
  }
};
