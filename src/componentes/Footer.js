import React from 'react'
import Styled from 'styled-components'
import Logo from '../img/logo.png'

const Footer = Styled.div`
    background:rgba(38,50,56,1);
    padding:20px;
`
export default ()=>(
  <Footer>
      <img src={Logo} alt="logo" width="200" height="75" />
  </Footer>
   
   
)