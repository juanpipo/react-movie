import React from 'react'
import ReactDOM from 'react-dom'

import App from './componentes/App';
import ServiceWorker from './serviceWorker';

import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle `
    @import url('https://fonts.googleapis.com/css?family=Amatic+SC|Anton|Rock+Salt|Cinzel');
    body {
        margin:0;
    }
`

ReactDOM.render( < App / > , document.getElementById('root'));