import React from 'react'
import axios from 'axios'
import Destaque from '../componentes/Destaque';
import Estrenos from '../componentes/Estrenos';
import Styled from 'styled-components';
import TituloSeccion from '../componentes/TituloSeccion';



class HomePage extends React.Component {
    state = {
        peliculas: [],
        peliculaDestacada:"",
        proximos_estrenos:[]
    }

    componentDidMount(){
        this.getData()
        this.getProximosEstrenos()
        console.log(this.props.test)
    }

    getProximosEstrenos = async ()=> {
        try {
            const resultados = await axios.get('https://api.themoviedb.org/3/movie/upcoming?api_key=19e8cfc90a18ecf11965ae3155e97e8c&language= es')  
            
            this.setState({
                proximos_estrenos:resultados.data.results
            })
        } catch (error) {
            console.log(error.message)
        }
    }

    getData = async ()=> {
        try {
            const resultados = await axios.get('https://api.themoviedb.org/3/movie/now_playing?api_key=19e8cfc90a18ecf11965ae3155e97e8c&language= es')  
            this.setPeliculaDestacada(resultados.data.results)
            this.setState({
                peliculas:resultados.data.results
            })
        } catch (error) {
            console.log(error.message)
        }
       
    }

    setPeliculaDestacada(peliculas) {
        const peliculaDestacada = peliculas[Math.floor(Math.random()*peliculas.length)]
        this.setState({
            peliculaDestacada
        })
    }

    render() {
        return (
            <div>
                <Destaque pelicula={this.state.peliculaDestacada} />
                <TituloSeccion>Estrenos:</TituloSeccion>
                <Estrenos peliculas = {this.state.peliculas} />
                <TituloSeccion>Proximamente:</TituloSeccion>
                <Estrenos peliculas = {this.state.proximos_estrenos} />
            </div>
        );
    }
}


export default HomePage; 